package com.twuc.webApp.Controller;

import com.twuc.webApp.RequestBodyMessage;
import org.springframework.web.bind.annotation.*;

import static com.twuc.webApp.Service.PlusAndMultiplyService.getPlusOrMultiplyStr;

@RestController
public class PlusAndMultiplyController {

    public String getMultiplyForm() {
        return getMultiplyForm(1, 9);
    }

    public String getPlusForm() {
        return getPlusForm(1, 9);
    }

    @GetMapping("/api/tables/plus")
    public String getPlusForm(@RequestParam(defaultValue = "1") int start, @RequestParam(defaultValue = "9") int end) {
        return getPlusOrMultiplyStr(start, end, "+");
    }

    @GetMapping("/api/tables/multiply")
    public String getMultiplyForm(@RequestParam(defaultValue = "1") int start, @RequestParam(defaultValue = "9") int end) {
        return getPlusOrMultiplyStr(start, end, "*");
    }

    @PostMapping("/api/check")
    public boolean check(@RequestBody RequestBodyMessage requestBodyMessage){
        switch (requestBodyMessage.getOperation()){
            case PLUS:
                if(requestBodyMessage.getOperandLeft() + requestBodyMessage.getOperandRight() == requestBodyMessage.getExpectedResult()){
                    return true;
                }
                break;
            case MULTIPLY:
                if(requestBodyMessage.getOperandLeft() * requestBodyMessage.getOperandRight() == requestBodyMessage.getExpectedResult()){
                    return true;
                }
                break;
        }
        return false;
    }
}
