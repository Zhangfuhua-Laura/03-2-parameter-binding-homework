package com.twuc.webApp.Service;

public class PlusAndMultiplyService {
    public static String getPlusOrMultiplyStr(int start, int end, String op){
        String str = "";
        for (int i = start; i <= end; i++) {
            for (int j = 1; j <= i; j++) {
                int sum;

                if(op.equals("+"))
                    sum = i + j;
                else
                    sum = i * j;
                str = str + i + op + j + "=" + sum;

                if (sum > 9)
                    str += " ";
                else
                    str += "  ";
            }
            str += "\n";
        }
        return str;
    }
}
