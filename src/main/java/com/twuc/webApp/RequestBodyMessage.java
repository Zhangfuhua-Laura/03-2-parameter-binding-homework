package com.twuc.webApp;

public class RequestBodyMessage {
    private int operandLeft;
    private int operandRight;
    private OperatorEnum operation;
    private int expectedResult;

    public int getOperandLeft() {
        return operandLeft;
    }

    public int getOperandRight() {
        return operandRight;
    }

    public OperatorEnum getOperation() {
        return operation;
    }

    public int getExpectedResult() {
        return expectedResult;
    }

    public RequestBodyMessage(int operandLeft, int operandRight, OperatorEnum operation, int expectedResult) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
    }

    public RequestBodyMessage() {
    }
}
